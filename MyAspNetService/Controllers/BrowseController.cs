﻿using MyAspNetService.Helpers;
using MyAspNetService.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Filters;

namespace MyAspNetService.Controllers
{
    public class BrowseController : ApiController
    {

        [AcceptVerbs("GET", "HEAD")]
        [Route("api/browse/{*paths}")]
        public HttpResponseMessage Get([FromUri]string paths)
        {
            string path = parseParam(paths);

            ParserPath parser = new ParserPath(path, this.Request.RequestUri.AbsoluteUri);
            HttpResponseMessage response;

            if (!parser.TryReadingFile(path, Request,  out response))
            {
                ResponseContext folderResponse = getResponseFolderContext(path, parser);
                if (folderResponse.Response == HttpStatusCode.OK)
                {
                    response = Request.CreateResponse<List<Info>>(folderResponse.ResponseBody);
                }
                else
                {
                    response = new HttpResponseMessage(folderResponse.Response);
                }
            }

            return response;
        }

        private string parseParam(string paths)
        {
            var pathSplit = paths.Split('/');
            string path = "";
            bool oneTime = true;
            for (int i = 0; i < pathSplit.Length; i++)
            {
                if (oneTime)
                {
                    path += pathSplit[i] + @":\";
                    oneTime = false;
                }
                else
                {
                    if (i == pathSplit.Length - 1)
                    {
                        path += pathSplit[i];
                    }
                    else
                    {
                        path += pathSplit[i] + @"\";
                    }
                }
            }

            return path;
        }

        private ResponseContext getResponseFolderContext(string path, ParserPath parser)
        {
            ResponseContext response = new ResponseContext();
            List<Info> responseBody = new List<Info>();
            try
            {
                parser.ReadingFolder((info) =>
                {
                    responseBody.Add(info);
                });

                if (responseBody.Any())
                {
                    response.Response = HttpStatusCode.OK;
                }
                else
                {
                    response.Response = HttpStatusCode.NoContent;
                }
            }
            catch (DirectoryNotFoundException ex)
            {
                response.Response = HttpStatusCode.NotFound;
            }
            response.ResponseBody = responseBody;
            return response;
        }
    }
}
