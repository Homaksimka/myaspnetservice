﻿using MyAspNetService.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.IO.MemoryMappedFiles;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Net;

namespace MyAspNetService.Helpers
{
    public sealed class ParserPath
    {
        private const string MapNamePrefix = "FileServerMap";

        public string Path { set; get; }
        public string Url { set; get; }

        public delegate void readingFunction(
            Info info);

        public ParserPath(string path, string url)
        {
            this.Path = path;
            this.Url = url;
        }

        public bool TryReadingFile(string path, HttpRequestMessage request, out HttpResponseMessage Response )
        {
            Response = new HttpResponseMessage();
            if (!File.Exists(path))
            {
                Response = null;
                return false;
            }
            var fileInfo = new FileInfo(path);
            long fileLength = fileInfo.Length;
            ContentInfo contentInfo = GetContentInfoFromRequest(request, fileLength);

            if (request.Method == HttpMethod.Get)
            {
                string mapName = GenerateMapNameFromName(fileInfo.Name);
                MemoryMappedFile mmf = null;
                try
                {
                    mmf = MemoryMappedFile.OpenExisting(mapName, MemoryMappedFileRights.Read);
                }
                catch (FileNotFoundException)
                {
                    mmf = MemoryMappedFile
                        .CreateFromFile(File.Open(path, FileMode.Open), mapName, fileLength,
                        MemoryMappedFileAccess.Read, null, HandleInheritability.None,
                        false);
                }

                using (mmf)
                {
                    Stream stream = contentInfo.IsPartial
                        ? mmf.CreateViewStream(contentInfo.From, contentInfo.Length, MemoryMappedFileAccess.Read)
                        : mmf.CreateViewStream(0, contentInfo.Length, MemoryMappedFileAccess.Read);

                    Response.Content = new StreamContent(stream);
                    
                }
            }
            else if (request.Method == HttpMethod.Head)
            {
                Response.Content = new ByteArrayContent(new byte[0]);
            }

            SetResponseHeaders(Response, contentInfo, fileLength, path);

            return true;
        }

        public void ReadingFolder(readingFunction func)
        {
            try
            {
                var subFolders = Directory.GetDirectories(Path);
                var files = Directory.GetFiles(Path);

                readSubfoldersInFolder(subFolders,
                    func);
                readFilesIntoFolder(files,
                    func);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void readSubfoldersInFolder(string[] subFolders, readingFunction func)
        {
            foreach (var folder in subFolders)
            {
                Info outInfo = new Info();
                var info = new DirectoryInfo(folder);
                outInfo.Name = info.Name;
                outInfo.Size = returnSizeOfSubFolders(folder).ToString();
                outInfo.CreatedDateTime = info.CreationTime.ToString();
                outInfo.LastModifiedDateTime = info.LastWriteTime.ToString();
                outInfo.Link = this.Url + "/" + info.Name;

                func(outInfo);
            }
        }

        private void readFilesIntoFolder(string[] files, readingFunction func)
        {
            foreach (var file in files)
            {
                Info outInfo = new Info();
                var info = new FileInfo(file);
                outInfo.Name = info.Name;
                outInfo.Size = info.Length.ToString();
                outInfo.CreatedDateTime = info.CreationTime.ToString();
                outInfo.LastModifiedDateTime = info.LastWriteTime.ToString();
                outInfo.ReadOnly = info.IsReadOnly.ToString();
                outInfo.Hidden = (File.GetAttributes(file) & FileAttributes.Hidden) == FileAttributes.Hidden ? "True" : "False";
                outInfo.Link = this.Url + "/" + info.Name;

                func(outInfo);

            }
        }

        private long returnSizeOfSubFolders(string path)
        {
            long rezult = 0;

            var files = Directory.GetFiles(path);
            var subFolders = Directory.GetDirectories(path);

            foreach (var folder in subFolders)
            {
                rezult += returnSizeOfSubFolders(folder);
            }

            foreach (var file in files)
            {
                var info = new FileInfo(file);
                rezult += info.Length;
            }

            return rezult;
        }

        private ContentInfo GetContentInfoFromRequest(HttpRequestMessage request, long entityLength)
        {
            var rezult = new ContentInfo
            {
                From = 0,
                To = entityLength,
                IsPartial = false,
                Length = entityLength
            };

            RangeHeaderValue rangeHeader = request.Headers.Range;
            if (rangeHeader != null && rangeHeader.Ranges.Count != 0)
            {
                if (rangeHeader.Ranges.Count > 1)
                {
                    throw new HttpResponseException(HttpStatusCode.RequestedRangeNotSatisfiable);
                }

                RangeItemHeaderValue range = rangeHeader.Ranges.First();
                if (range.From.HasValue && range.From < 0
                    || range.To.HasValue && range.To > entityLength - 1)
                {
                    throw new HttpResponseException(HttpStatusCode.RequestedRangeNotSatisfiable);
                }

                rezult.From = range.From ?? 0;
                rezult.To = range.To ?? entityLength - 1;
                rezult.IsPartial = true;
                rezult.Length = entityLength;
                if (range.From.HasValue && range.To.HasValue)
                {
                    rezult.Length = range.To.Value - range.From.Value + 1;
                }
                else if (range.From.HasValue)
                {
                    rezult.Length = entityLength - range.From.Value + 1;
                }
                else if (range.To.HasValue)
                {
                    rezult.Length = range.To.Value + 1;
                }
            }

            return rezult;
        }

        private void SetResponseHeaders(HttpResponseMessage response, ContentInfo contentInfo,
            long fileLength, string fileName)
        {
            response.Headers.AcceptRanges.Add("bytes");
            response.StatusCode = contentInfo.IsPartial ? HttpStatusCode.PartialContent
                : HttpStatusCode.OK;
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentDisposition.FileName = fileName;
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            response.Content.Headers.ContentLength = contentInfo.Length;
            if (contentInfo.IsPartial)
            {
                response.Content.Headers.ContentRange = new ContentRangeHeaderValue(contentInfo.From, contentInfo.To, fileLength);

            }
        }

        private string  GenerateMapNameFromName(string fileName)
        {
            return String.Format("{0}_{1}", MapNamePrefix, fileName);
        }

    }
}