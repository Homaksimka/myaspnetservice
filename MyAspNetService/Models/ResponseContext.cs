﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;

namespace MyAspNetService.Models
{
    public class ResponseContext
    {
        public HttpStatusCode Response { get; set; }
        public List<Info> ResponseBody { get; set; }
    }
}