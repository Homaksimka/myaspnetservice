﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyAspNetService.Models
{
    public class Info
    {
        public string Name { get; set; }
        public string Size { get; set; }
        public string CreatedDateTime { get; set; }
        public string LastModifiedDateTime { get; set; }
        public string ReadOnly { get; set; }
        public string Hidden { get; set; }
        public string Link { get; set; }
    }
}